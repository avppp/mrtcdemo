#include "Mp4File.h"
#include <iostream>
#include <chrono>
#include <thread>
namespace mrtclinux {

#define MAX_AUDIO_FRAME_SIZE  192000    //48Khz 32bit 1 seconds
#define kNumNanosecsPerSecond 1000000000LL
#define SAMPLE_PRT(fmt...)   \
    do {\
        printf("[%s]-%d: ", __FUNCTION__, __LINE__);\
        printf(fmt);\
       }while(0)

Mp4Parse::Mp4Parse(const std::string& mp4File,AvDataCallback* callback) :
            mp4File_(mp4File) , callback_(callback)
{
    Init();
}

Mp4Parse::~Mp4Parse() {
    Stop();
}

int Mp4Parse::Init() {
    //av_register_all();
    formatCtx_ = avformat_alloc_context();
    if ((avformat_open_input(&formatCtx_, mp4File_.c_str(), 0, 0)) < 0) {
        SAMPLE_PRT( "Could not open input file\n");
        return -1;
    }
    if (avformat_find_stream_info(formatCtx_, 0) < 0) {
        SAMPLE_PRT( "Failed to retrieve input stream information\n");
        return -1;
    }
    for(int i=0; i<formatCtx_->nb_streams; i++) {
        if(formatCtx_->streams[i]->codecpar->codec_type==AVMEDIA_TYPE_VIDEO){
            videoStreamIdx_=i;
        }else if(formatCtx_->streams[i]->codecpar->codec_type==AVMEDIA_TYPE_AUDIO){
            audioStreamIdx_=i;
        }
    }
    av_dump_format(formatCtx_, 0, mp4File_.c_str(), 0);

    videoCodec_ = avcodec_find_decoder(formatCtx_->streams[videoStreamIdx_]->codecpar->codec_id);
    videoCodecCtx_ = avcodec_alloc_context3(videoCodec_);
    avcodec_parameters_to_context(videoCodecCtx_,formatCtx_->streams[videoStreamIdx_]->codecpar);
    if(avcodec_open2(videoCodecCtx_,videoCodec_,NULL) < 0) {
        SAMPLE_PRT("avcodec_open2 open video codec fail\n");
        return -1;        
    }
    AVRational fps = av_guess_frame_rate(formatCtx_,formatCtx_->streams[videoStreamIdx_],NULL);
    videoFps_ = fps.num/fps.den;
    videoWidth_ = videoCodecCtx_->width;
    videoHeight_ = videoCodecCtx_->height;
    frameIntervelNanos_ = kNumNanosecsPerSecond / videoFps_;

    audioCodecCtx_= avcodec_alloc_context3(NULL);
    avcodec_parameters_to_context(audioCodecCtx_,formatCtx_->streams[audioStreamIdx_]->codecpar);
    audioCodec_ = avcodec_find_decoder(audioCodecCtx_->codec_id);
    if (audioCodec_ == NULL) {
        SAMPLE_PRT("find audio codec fail\n");
        return -1;
    }
    if(avcodec_open2(audioCodecCtx_,audioCodec_,NULL)<0)
    {
        SAMPLE_PRT("avcodec_open2 open audio codec fail\n");
        return -1;
    }

    SetOutputPcmParams(audioCodecCtx_);

    const AVBitStreamFilter* filter = av_bsf_get_by_name("h264_mp4toannexb");
    av_bsf_alloc(filter,&h264bsfc_);
    avcodec_parameters_from_context(h264bsfc_->par_in,videoCodecCtx_);
    int ret = av_bsf_init(h264bsfc_);
    std::cout << "av_bsf_init ret " << ret << std::endl;

    h264_ = fopen("h264.raw","wb+");
    bsf_ = fopen("h264.bsf","wb+");
    return 0;
}

void Mp4Parse::Start() {
    //编码后的音视频
    AVPacket* pkt = av_packet_alloc();
    av_init_packet(pkt);

    //从pkt中得到的视频nalu
    uint8_t *h264_buf;
    int h264_len;

    //解码pkt得到的音频(AAC),从frame(PCM)拷贝到buffer
     int got_picture;
    AVFrame *frame = av_frame_alloc();
    uint8_t *buffer = (uint8_t *)av_malloc(MAX_AUDIO_FRAME_SIZE * 2);

    //buffer中的音频重采样为16K单声道(PCM)
    uint8_t *pcm_buf = (uint8_t *)av_malloc(MAX_AUDIO_FRAME_SIZE * 2);
    int pcm_len = 0;

    std::chrono::high_resolution_clock clock;
    auto lastTime = clock.now();
    while(av_read_frame(formatCtx_, pkt)>=0){
        if(pkt->stream_index==videoStreamIdx_){
            std::cout << "read video frame pts " << pkt->pts << ",dts " << pkt->dts << ",size " << pkt->size 
                               <<  ",w:" << videoWidth_ << ",h:" << videoHeight_ << std::endl;
            fwrite(pkt->data,1,pkt->size,h264_);
            int ret = av_bsf_send_packet(h264bsfc_,pkt);
            if(ret == 0) {
                while((ret = av_bsf_receive_packet(h264bsfc_,pkt)) == 0) {
                    //解码是很快的,模拟摄像头的采集行为,根据帧率控制输入速率
                    auto ms = std::chrono::duration_cast<std::chrono::nanoseconds>(clock.now()-lastTime);
                    auto delay = frameIntervelNanos_ - ms.count();
                    if(delay > 0) {
                        std::this_thread::sleep_for(std::chrono::nanoseconds(delay));
                    }
                    fwrite(pkt->data,1,pkt->size,bsf_);
                    callback_->OnVideoFrame(pkt->data,pkt->size,videoWidth_,videoHeight_);
                    lastTime = clock.now();
                }
            }
        }else if(pkt->stream_index==audioStreamIdx_){
            if (avcodec_decode_audio4(audioCodecCtx_,frame, &got_picture, pkt) < 0) {
                SAMPLE_PRT("error  avcodec_decode_audio4\n");
                return ;
            }
            if(got_picture>0)
            {
                int ret = swr_convert(audioSwrCtx_, 
                                            &buffer, 
                                            MAX_AUDIO_FRAME_SIZE*2,
                                            (const uint8_t **)frame->data, 
                                            frame->nb_samples);
                if(ret > 0) {
                    memcpy(pcm_buf+pcm_len,buffer,2*ret);
                    pcm_len+=2*ret;
                }
                //模拟音频采集设备行为，10ms,10ms的喂音频数据
                //为了测试而已，最后一次没喂完的音频数据不考虑了
                while(pcm_len > outAudioSampleSize_) {
                    callback_->OnAudioFrame(pcm_buf,outAudioSampleSize_);
                    pcm_len-=outAudioSampleSize_;
                    memmove(pcm_buf,pcm_buf+outAudioSampleSize_,pcm_len);
                }
            }
           got_picture=0;
            std::cout << "read audio frame pts " << pkt->pts << ",dts " << pkt->dts << ",size " << pkt->size << std::endl;
        }
       av_packet_unref(pkt);
    }
    if(buffer){
        av_freep(buffer);
    }
    if(pcm_buf){
        av_freep(pcm_buf);
    }
    av_frame_free(&frame);
    av_packet_free(&pkt);
}

void Mp4Parse::Stop() {
    if(h264bsfc_) {
        av_bsf_free(&h264bsfc_);
        h264bsfc_ = nullptr;
    }
    if(formatCtx_) {
        avformat_close_input(&formatCtx_);
        formatCtx_ = nullptr;
    }
    if(audioSwrCtx_) {
        swr_free(&audioSwrCtx_);
        audioSwrCtx_ = nullptr;
    }
    if(h264_){
        fclose(h264_);
        h264_=nullptr;
    }
    if(bsf_){
        fclose(bsf_);
        bsf_=nullptr;
    }
}
//设置音频解码为PCM后，重采样的格式
void Mp4Parse::SetOutputPcmParams(AVCodecContext* inCodecCtx) {
    //采样的布局方式
    uint64_t out_channel_layout = AV_CH_LAYOUT_MONO;   //单通道
    //采样个数
    int out_nb_samples = 160;
    //采样格式
    enum AVSampleFormat  out_sample_fmt = AV_SAMPLE_FMT_S16;
    //采样率
    int out_sample_rate = 16000;
    //通道数
    int out_channels = av_get_channel_layout_nb_channels(out_channel_layout);
    
    int64_t in_channel_layout = av_get_default_channel_layout(inCodecCtx->channels);
    //打开转码器
    audioSwrCtx_ = swr_alloc();
    //设置转码参数
    audioSwrCtx_ = swr_alloc_set_opts(audioSwrCtx_, 
                                                                                out_channel_layout, 
                                                                                out_sample_fmt, 
                                                                                out_sample_rate, 
                                                                                in_channel_layout, 
                                                                                inCodecCtx->sample_fmt, 
                                                                                inCodecCtx->sample_rate, 
                                                                                0, NULL);
    //初始化转码器
    swr_init(audioSwrCtx_);

    outAudioSampleSize_ = av_samples_get_buffer_size(NULL, out_channels, out_nb_samples, out_sample_fmt, 1);
}

}
