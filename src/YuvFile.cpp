#include "YuvFile.h"

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <iostream>

namespace mrtclinux {

YuvFile::YuvFile()
    : yuv_file_(NULL),
      end_of_file_(false),
      auto_rewind_(false),
      rewinded_(false),
      read_idx_(0),
      total_frames_(0) {
}

YuvFile::~YuvFile() {
  Close();
}

void YuvFile::Open(const std::string& filename,const char* mode) {
  if ((yuv_file_ = fopen(filename.c_str(), mode)) == NULL) {
    std::cout << "Cannot open file "  << filename << std::endl;
  }
}

void YuvFile::Open(const std::string& filename,size_t width,size_t height,const char* mode,bool auto_rewind) {
  if ((yuv_file_ = fopen(filename.c_str(), mode)) == NULL) {
    std::cout << "Cannot open file "  << filename << std::endl;
    return;
  }
  width_ = width;
  height_ = height;
  auto_rewind_ = auto_rewind_;

  fseek(yuv_file_,0,SEEK_END);
  const size_t file_size = ftell(yuv_file_);
  const int i420_frame_size = 3*width_*height_ / 2;
  total_frames_ = file_size / i420_frame_size ;
  fseek(yuv_file_,0,SEEK_SET);

  auto_rewind_ = auto_rewind;
  end_of_file_ = false;
  rewinded_ = false;

  std::cout << "Video has " << total_frames_ << " frames, width: " << width_ << ", height:" << height_ << std::endl;
}

  RtcVideoData YuvFile::ReadFrame() {
    RtcVideoData video;
    if(read_idx_ == total_frames_) {
      if(auto_rewind_) {
        Rewind();
      } else if(feof(yuv_file_)) {
        std::cout << "End of file reached!" << std::endl;
        return video;
      } else {
        std::cout << "ReadFrame error!" << std::endl;
        return video;
      }
    }

    int half_width = (width_ +1 ) / 2;
    size_t size_y = static_cast<size_t>(width_)*height_;
    size_t size_uv = static_cast<size_t>(half_width) *((height_ +1) / 2) ;

    uint8_t* data_y = new uint8_t[size_y] ;
    uint8_t* data_u = new uint8_t[size_uv] ;
    uint8_t* data_v = new uint8_t[size_uv] ;

    // fpos_t startPos = frame_positions_[read_idx_];
    // fsetpos(yuv_file_,&startPos);
    fread(data_y,1,size_y,yuv_file_);
    fread(data_u,1,size_uv,yuv_file_);
    fread(data_v,1,size_uv,yuv_file_);

    // int ylen = size_y;
    // int uvlen = size_uv;
    // printf("------------------yyyy--------------\n");
    // for(int i=0;i<ylen;i++){
    // printf("%x",*(data_y + i));
    // }
    // printf("------------------uuuu--------------\n");
    // for(int i=0;i<uvlen;i++){
    // printf("%x",*(data_u + i));
    // }
    // printf("------------------vvvv--------------\n");
    // for(int i=0;i<uvlen;i++){
    // printf("%x",*(data_v + i));
    // }

    video.dataY = data_y;
    video.dataU = data_u;
    video.dataV = data_v;
    video.length = size_y + 2*size_uv;
    video.width = width_;
    video.height = height_;
    video.strideY = width_;
    video.strideU = half_width;
    video.strideV = half_width;
    
    std::cout << "Read frame ok idx :" << read_idx_ << ",w:" << width_ << ",h:" << height_ << std::endl;
    read_idx_++;
    return video;
  }

  void YuvFile::WriteFrame(const RtcVideoData& video) {
    uint8_t *pos  = nullptr;
    int half_width = (video.width +1 ) / 2;
    int half_height = (video.height +1) /2;
    size_t size_y = static_cast<size_t>(video.width)*video.height;
    size_t size_uv = static_cast<size_t>(half_width) *((video.height +1) / 2) ;
    if(video.strideY == video.width) {
      fwrite(video.dataY,1,size_y,yuv_file_);
      fwrite(video.dataU,1,size_uv,yuv_file_);
      fwrite(video.dataV,1,size_uv,yuv_file_);
    } else {
      pos = const_cast<uint8_t*>(video.dataY);
      for(int i=0;i<video.height;i++) {
        fwrite(pos,1,video.width,yuv_file_);
        pos += video.strideY;
      }
      pos = const_cast<uint8_t*>(video.dataU);
      for(int i=0;i<half_height;i++) {
        fwrite(pos,1,half_width,yuv_file_);
        pos += video.strideU;
      }
      pos = const_cast<uint8_t*>(video.dataV);
      for(int i=0;i<half_height;i++) {
        fwrite(pos,1,half_width,yuv_file_);
        pos += video.strideV;
      }
    }
    std::cout << "Write frame ok idx :" << total_frames_ << ",w:" << video.width << ",h:" << video.height << std::endl;
    total_frames_ ++;
  }

void YuvFile::Close() {
  if (yuv_file_) {
    fclose(yuv_file_);
  }
  read_idx_ = 0;
  total_frames_ = 0;
  frame_positions_.clear();
}

void YuvFile::Rewind() {
  rewind(yuv_file_);
  end_of_file_ = false;
  read_idx_ = 0;
}

bool YuvFile::Rewinded() {
  return rewinded_;
}

}  // namespace webrtc
